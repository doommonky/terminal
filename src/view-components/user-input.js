import React, {useState, useEffect} from 'react';
import '../App.css'

export default function UserInput(props) {
    useEffect(() => {
        let input = document.getElementById('inputBox');
        setInputBoxWidth(input);
    })

    function onSubmit(event){
        if (props.any){
            props.loadArea(props.next);
            return;
        }
        
        if(event.key !== "Enter")
            return;

        let input = document.getElementById('inputBox');
        props.loadArea(input.value);
        input.value="";
        setInputBoxWidth(input);
    }

    function onChange(){
        let input = document.getElementById('inputBox');
        setInputBoxWidth(input);
    }

    function setInputBoxWidth(input){
        input.style.width=input.value.length+'ch';
    }

    return(
        <div className='Input'>
            <label>></label>
            <input autoFocus onBlur={({target}) => target.focus()} id='inputBox' type='text' className='InputBox' onChange={onChange} onKeyDownCapture={onSubmit}/>
        </div>
    )
}