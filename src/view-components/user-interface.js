import React, {useState} from "react";
import TextDisplay from "./text-display";
import UserInput from "./user-input";
import areasRaw from "../area-content/areas.json"

export default function UserInterface() {
    const [areaId, setAreaId] = useState(0)
    const areas = areasRaw.map((area => {
        return area;
    }))

    let loadArea = (areaId) => {
        setAreaId(areaId);
    }

	return (
		<div className="App">
			<TextDisplay area={areas[areaId]} />
			<br />
			<UserInput loadArea={loadArea} any={areas[areaId].any} next={areas[areaId].next}/>
		</div>
	);
}
