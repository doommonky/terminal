import React, { useState, useEffect } from "react";
import "../App.css";

export default function TextDisplay(props) {
	const [displayText, setDisplayText] = useState("");
	const [currentIndex, setCurrentIndex] = useState(0);

	useEffect(() => {
        if (currentIndex - 1 === props.area.displayText.length)
            return;

        const tick = setInterval(() => {
            setDisplayText(props.area.displayText.substring(0, currentIndex));
            setCurrentIndex(currentIndex + 1);
        }, 50);
        return () => {
            clearInterval(tick);
        }
	});

	return <div className="Display">{displayText}</div>;
}
