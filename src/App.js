import React from 'react';
import './App.css';
import UserInterface from './view-components/user-interface';

function App() {
  return (
    <UserInterface/>
  );
}

export default App;
